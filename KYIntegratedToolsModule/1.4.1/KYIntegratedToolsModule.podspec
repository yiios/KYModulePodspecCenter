#
#  Be sure to run `pod spec lint KYIntegratedToolsModule.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "KYIntegratedToolsModule"
  s.version      = "1.4.1"
  s.summary      = "内部app公用组件，包括所有工具类、数据库管理、公用UI以及用户数据."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = <<-DESC
            内部app公用组件，包括所有工具类、数据库管理、公用UI以及用户数据.
                   DESC

  s.homepage     = "http://583004971.com"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "xiegang" => "xieganghome@163.com" }
  # Or just: s.author    = "xiegang"
  # s.authors            = { "xiegang" => "" }
  # s.social_media_url   = "http://twitter.com/xiegang"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # s.platform     = :ios
    s.platform     = :ios, "7.0"

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "http://10.85.85.47/iOSInternalProjectTeam/ComponentLibrary/KYIntegratedToolsModule.git", :tag => "1.4.1" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  # s.source_files  = "Classes", "Classes/**/*.{h,m}"
  # s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

    s.requires_arc = true
 s.subspec 'KYBasisUI' do |ss|
        ss.source_files = 'KYIntegratedToolsModule/KYBasisUI/**/*'
        ss.public_header_files = 'KYIntegratedToolsModule/KYBasisUI/**/*.h'
        ss.dependency  "KYClassExtensions", "~> 1.2.1"
 end

 s.subspec 'ThirdPartySupportLib' do |ss|
        ss.source_files = 'KYIntegratedToolsModule/ThirdPartySupportLib/**/*'
        ss.public_header_files = 'KYIntegratedToolsModule/ThirdPartySupportLib/**/*.h'
 end

 s.subspec 'KYAllTools' do |ss|
        ss.source_files = 'KYIntegratedToolsModule/KYAllTools/**/*'
        ss.public_header_files = 'KYIntegratedToolsModule/KYAllTools/**/*.h'
        ss.dependency  "BlocksKit", "~> 2.2.5"
        ss.dependency  "RDVTabBarController", "~> 1.1.9"
        ss.dependency  "AFNetworking", "~> 3.0"
        ss.dependency  "RDVTabBarController", "~> 1.1.9"
        ss.dependency  "BaiduMapKit", "~> 3.0.0"
        ss.dependency  "KYClassExtensions", "~> 1.2.1"
        ss.dependency  "MBProgressHUD", "~> 0.9.1"
        ss.ios.vendored_libraries = "KYIntegratedToolsModule/ThirdPartySupportLib/*.a"
        ss.dependency  'KYIntegratedToolsModule/KYBasisUI'
        ss.dependency  'KYIntegratedToolsModule/ThirdPartySupportLib'
 end

 s.subspec 'KYDatabaseTools' do |ss|
        ss.source_files = 'KYIntegratedToolsModule/KYDatabaseTools/**/*'
        ss.public_header_files = 'KYIntegratedToolsModule/KYDatabaseTools/**/*.h'
        ss.dependency 'FMDB', '~> 2.6'
 end

end
